<?php include('config.php');
$url = isset($_GET['url']) ? $_GET['url'] : 'home';?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	<link rel="stylesheet" href="<?php echo INCLUDE_PATH?>css/style.css" type="text/css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<?php if($url == 'home'){?>
	<script type="text/javascript" src="<?php echo INCLUDE_PATH?>js/main-functions.js"></script>
<?php }else{ ?>
	<script type="text/javascript" src="<?php echo INCLUDE_PATH?>js/functions.js"></script>
<?php } ?>
</head>
<body>
	<base base="<?php echo INCLUDE_PATH?>">
	<header class="container">
		<?php 
		
		if($url == 'home'){?>
		<h1>Product List</h1>
		<nav>
			<ul>
				<li><button name="add-product" id="add-product-btn" value="add">ADD</button></li>
				<li><button name="deleteButton" form="massDel" id="delete-product-btn">MASS DELETE</button></li>
			</ul>
		</nav>
		
		
	<?php }else{?>
		<h1>Product Add</h1>
		<nav>
			<ul>
				<li><button type="submit" form="product_form" name="save" id="save-product-btn">Save</button></li>
				<li><button id="cancel-product-btn">Cancel</button></li>
			</ul>
		</nav>
	<?php } ?>
	<div class="clear"></div>
	<hr>
	</header>
	
	<main class="flex container">
		<?php
		    if(file_exists('pages/'.$url.'.php')){
				require('pages/'.$url.'.php');
			}else{
				echo 'error';
			}

		?>
	
	</main>
	<footer class="container">
		<hr>
		<p>Scandiweb Test assignment</p>
	</footer>
</body>
</html>