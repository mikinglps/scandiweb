<?php
	include('../config.php');
	$data = array();
	@$sku = @$_POST['sku'];
	@$name = @$_POST['nameItem'];
	@$price = @$_POST['price'];
	@$type = @$_POST['type'];
	@$size = @$_POST['size'];
	@$weight = @$_POST['weight'];
	@$height = @$_POST['height'];
	@$width = @$_POST['width'];
	@$length = @$_POST['length'];
	$query = MySql::connect()->prepare("SELECT id FROM `tb_type` WHERE name = ?");
	$query->execute(array($type));
	$query = $query->fetch();
	$count = MySql::connect()->prepare("SELECT sku FROM `tb_products` WHERE sku = ?");
	$count->execute(array($sku));
	$count = $count->rowCount();
	if($sku == '' OR $name == '' OR $price == '' OR $type == ''){
		$data['error'] = true;
	}elseif($count > 0){
		$data['repeat'] = true;
	}elseif($type == 'dvd'){

		$sql = MySql::connect()->prepare("INSERT INTO `tb_products` VALUES (?,?,?,?,?,null,null,null,null)");
		if($sql->execute(array($sku,$name,$price,$query[0],$size))){
			$data['success'] = true;
		}else{
			$data['error'] = true;
		}
	}elseif($type == 'book'){
		$sql = MySql::connect()->prepare("INSERT INTO `tb_products` VALUES (?,?,?,?,null,?,null,null,null)");
		if($sql->execute(array($sku,$name,$price,$query[0],$weight))){
			$data['success'] = true;
		}else{
			$data['error'] = true;
		}
	}elseif($type == 'furniture'){
		$sql = MySql::connect()->prepare("INSERT INTO `tb_products` VALUES (?,?,?,?,null,null,?,?,?)");
		if($sql->execute(array($sku,$name,$price,$query[0],$height,$width,$length))){
			$data['success'] = true;
		}else{
			$data['error'] = true;
		}
	}

die(json_encode($data));


	
?>