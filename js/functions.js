window.addEventListener('load',function(){

	var type = document.querySelector('#productType');
	var open = false;
	var allOpt = document.querySelectorAll('[opt]');
	
	type.addEventListener('change',()=>{
	if(open == true){
		for(var i = 0; i < allOpt.length;i++){
			allOpt[i].style.display = 'none';
		}
		open = false;
	}
	})

	type.addEventListener('change',()=>{
	var whatType = type.value;
	var opt = document.querySelector('[opt='+whatType+']').getAttribute('opt');
	var whatOpt = document.querySelector('[opt='+whatType+']');
	
	if(opt == whatType && open == false){
	whatOpt.style.display = 'block';
	open = true;
	}
})


	$('#product_form').submit(function(){
		var form = $(this);
		$.ajax({
			url: 'ajax/request.php',
			method: 'post',
			dataType:'json',
			data:form.serialize()
		}).done(function(data){
			if(data.error){
				alert('Please, submit required data');
			}else if(data.success){
				window.location.href="home";
			}else if(data.repeat){
				alert('SKU Replicated');
			}
		});
		return false;
	})

	$('#cancel-product-btn').click(function(){
		window.location.href="home";
	})

	})

