<?php
date_default_timezone_set('America/Sao_Paulo');
$autoload = function($class){
	include('classes/'.$class.'.php');
};
spl_autoload_register($autoload);
define('HOST','localhost:3306');
define('DB','scandiweb-test');
define('USER','root');
define('PASS','');
define('INCLUDE_PATH','http://localhost/scandiweb_test/');
?>