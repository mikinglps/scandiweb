<form id="product_form" method="post">

	<table>
	<tr>
	<td><p>SKU</p>
	<td><input id="sku" type="text" name="sku" placeholder="Please, provide SKU"></td>
	</tr>
	<tr>
	<td><p>Name</p></td>
	<td><input required id="name" type="text" name="nameItem" placeholder="Please, provide the name"></td>
	</tr>
	<tr>
	<td><p>Price ($)</p></td>
	<td><input required id="price" type="number" name="price" placeholder="Please, provice the price"></td>
	</tr>
	<tr>
	<td><p>Type</p></td>
	<td><select required id="productType" name="type">
		<option value="">Please, select one</option>
		<option id="dvd" value="dvd">DVD</option>
		<option id="book" value="book">Book</option>
		<option id="furniture" value="furniture">Furniture</option>
	</select></td>
	</tr>
	<tr opt="dvd">
	<td><p>Size (MB)</p></td>
	<td><input id="size" type="number" name="size" placeholder="e.g: 512"/></td>
	</tr>
	<tr opt="book">
	<td><p>Weight (KG)</p></td>
	<td><input id="weight" type="number" name="weight" placeholder="e.g: 70"></td>
	</tr>
	<tr opt="furniture">
	<td><p>Height (CM)</td>
	<td><input id="height" type="number" name="height" placeholder="e.g: 30"></td>
	<td><p>Width (CM)</td>
	<td><input id="width" type="number" name="width" placeholder="e.g: 50"></td>
	<td><p>Length (CM)</p></td>
	<td><input id="length" type="number" name="length" placeholder="e.g: 60"></td>
	</tr>
	</table>
</form>