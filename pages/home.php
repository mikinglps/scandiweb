
	<form method="post" class="flex" id="massDel">
		<?php 
	$items = MySql::connect()->prepare("SELECT sku,name,price,type_id FROM `tb_products`");
	$items->execute();
	$items = $items->fetchAll();
	foreach($items as $key => $value){
		$findType = MySql::connect()->prepare("SELECT name FROM `tb_type` WHERE id = ?");
		$findType->execute(array($value['type_id']));
		$type = $findType->fetch();?>
		<div class="single-product">
			
				<input class="delete-checkbox" type="checkbox" name="massDel[]" value="<?php echo $value['sku']?>">
			
			<p><?php echo $value['sku']?></p>
			<p><?php echo $value['name']?></p>
			<p>$<?php echo $value['price']?></p>
			<p><?php echo $type['name']?></p>
		</div>
	<?php }
	if(isset($_POST['deleteButton'])){
	$selections = $_POST['massDel'];
	for($i = 0;$i<count($selections);$i++){
		$del = MySql::connect()->prepare("DELETE FROM `tb_products` WHERE sku = ?");
		$del->execute(array($selections[$i]));
	}
	header('Location: '.INCLUDE_PATH);
} ?>

	</form>